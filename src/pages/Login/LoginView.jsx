import "../../assets/styles/Auth.scss"

const LoginView = ({ handleForm }) => {
  return (
    <div className="auth-page">
      <form id="login_form" name="login_form" className="auth_formAuth" onSubmit={(e) => handleForm(e)}>
        <h1 className="auth_formAuth__title">Авторизация</h1>
        <div className="auth_groupBlock">
          <div className="auth_groupBlock__row">
            <input type="text" id="login_name" name="login_form[name]" placeholder="Ваше имя" autoComplete="true" className="auth_groupBlock__field" />
          </div>
          <button className="button">Авторизоваться</button>
        </div>
      </form>
    </div>
  );
};

export default LoginView;
