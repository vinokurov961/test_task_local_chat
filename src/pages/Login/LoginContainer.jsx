import LoginView from "./LoginView";
import { useDispatch } from "react-redux";
import { setUserData } from "../../redux/actions/UserActions";

const LoginContainer = () => {
  const dispatch = useDispatch();

  const handleSubmitFormLogin = (event) => {
    event.preventDefault();

    const elementLoginName = event.target.login_name,
      valueLoginName = elementLoginName.value.trim(),
      localStorageUsersList = JSON.parse(localStorage.getItem("users")) || [];

    if (valueLoginName === "") return alert("Введите имя");
    if (valueLoginName.length < 3) return alert("Имя должно быть больше двух букв");

    if (!localStorageUsersList.find((user) => user.name === valueLoginName)) {
      localStorageUsersList.push({
        name: valueLoginName
      });
  
      localStorage.setItem("users", JSON.stringify(localStorageUsersList))
    }


    dispatch(setUserData(valueLoginName));
  };

  return <LoginView handleForm={handleSubmitFormLogin} />;
};

export default LoginContainer;
