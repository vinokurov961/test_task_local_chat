import { useEffect, useRef, useState } from "react";
import ChatView from "./ChatView";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";

const ChatContainer = (props) => {
  const { user } = useSelector((state) => state);
  const [chatsList, setChatsList] = useState([]);
  const [currentChatMessages, setCurrentChatMessages] = useState({
    chatId: null,
    messages: [],
  });
  const [openSmileBox, setOpenSmileBox] = useState(false);
  const [searchingChatList, setSearchingChatList] = useState("");
  const messageFieldRef = useRef();
  const fileUploadRed = useRef();
  const searchFieldRef = useRef();
  const chatMessageWrapperRef = useRef();
  const [quotingMessage, setQuotingMessage] = useState({
    active: false,
    message: {},
  });

  useEffect(() => {
    setChatsList(getLocalStorageChatsList());

    if (currentChatMessages.chatId) {
      setCurrentChatMessages({
        ...currentChatMessages,
        messages: JSON.parse(localStorage.getItem("chatsMessagesList"))[currentChatMessages.chatId] || [],
      });
    }

    // Листнер для отслеживания измнений в локалсторедже
    const storageEventHandler = (event) => {
      const chatsList = getLocalStorageChatsList();
      const chatsMessagesList = JSON.parse(localStorage.getItem("chatsMessagesList"));

      if (chatsList.length) setChatsList(chatsList);

      if (chatsMessagesList && currentChatMessages.chatId) {
        setCurrentChatMessages({
          ...currentChatMessages,
          messages: chatsMessagesList[currentChatMessages.chatId] || [],
        });
      }
    };

    window.addEventListener("storage", (e) => storageEventHandler(e));

    slideToBottomChatMessage();

    return () => {
      window.removeEventListener("storage", (e) => storageEventHandler(e));
    };
  }, [currentChatMessages.chatId]);

  useEffect(() => {
    document.body.classList.add("page-chat");

    document.body.addEventListener("keydown", (e) => closeCurrentChat(e));

    return () => {
      window.removeEventListener("keydown", (e) => closeCurrentChat(e));
    };
  }, []);

  const closeCurrentChat = (event) => {
    if (event.code !== "Escape") return;

    setQuotingMessage({
      active: false,
      message: {},
    });
    setOpenSmileBox(false);
    setCurrentChatMessages({
      chatId: null,
      messages: [],
    });
  };

  // Функция для получения чат-листа для навбара, которая сортирует их по дате
  function getLocalStorageChatsList() {
    const localStorageChatsList = JSON.parse(localStorage.getItem("chatsList"));
    return localStorageChatsList
      ? localStorageChatsList.sort(
        (firstChat, secondChat) => new Date(secondChat.lastDateMessage) - new Date(firstChat.lastDateMessage)
      )
      : [];
  }

  // Создание нового чата и сохранения его в локалсторедж
  const createNewChat = () => {
    let chatsList = getLocalStorageChatsList();
    let chatsMessagesList = JSON.parse(localStorage.getItem("chatsMessagesList")) || {};
    let chatName = prompt("Укажите название чата", "NoName");
    let idNewChat = uuidv4();

    const generateLightColorRgb = () => {
      const red = Math.floor(((1 + Math.random()) * 256) / 1.5);
      const green = Math.floor(((1 + Math.random()) * 256) / 1.5);
      const blue = Math.floor(((1 + Math.random()) * 256) / 1.5);
      return `linear-gradient(-45deg, rgb(${Math.floor(red / 2)}, ${Math.floor(green / 2)}, ${Math.floor(
        blue / 2
      )}), rgb(${red}, ${green}, ${blue}))`;
    };

    chatsList.unshift({
      id: idNewChat,
      chatName: chatName.trim(),
      lastMessage: "".trim(),
      lastTypeMessage: "text",
      lastDateMessage: String(new Date()),
      chatAvatarColor: generateLightColorRgb(),
    });

    chatsMessagesList[idNewChat] = [];

    localStorage.setItem("chatsList", JSON.stringify(chatsList));
    localStorage.setItem("chatsMessagesList", JSON.stringify(chatsMessagesList));
    setChatsList(chatsList);
    setCurrentChatMessages({
      chatId: idNewChat,
      messages: [],
    });
  };

  // Открытие конкретного чата в навбаре и получения сообщений из локалстореджа
  const openChat = (chatId) => {
    let chatsMessagesList = JSON.parse(localStorage.getItem("chatsMessagesList")) || {};

    setCurrentChatMessages({
      chatId,
      messages: chatsMessagesList[chatId] || [],
    });

    closeQuotingMessage();
  };

  // Отправка сообщения по нажатию кнопки enter
  const handleKeySendMessage = (event) => {
    if (event.key !== "Enter") return;

    const newMessageText = event.target.value.trim();

    sendMessage(newMessageText, "text");

    event.target.value = "";
  };

  // Отправка сообщения по нажатию кнопки enter
  const handleButtonSendMessage = () => {
    sendMessage(messageFieldRef.current.value.trim(), "text");
    messageFieldRef.current.value = "";
  };

  // Фцнкция отправки сообщения, которая принимает только сообщение и тип сообщения
  // Сохраняет значение в локалсторежэ
  const sendMessage = (newMessageText, type) => {
    if (!currentChatMessages.chatId) return;

    if (newMessageText === "") return;

    const currentDateSendMessage = String(new Date());

    const dataNewMessage = {
      id: uuidv4(),
      authorName: user.name,
      authorId: user.id,
      text: newMessageText,
      textType: type,
      date: currentDateSendMessage,
      quoting: quotingMessage.active ? quotingMessage.message : {},
    };

    let chatsMessagesList = JSON.parse(localStorage.getItem("chatsMessagesList")) || {};
    let chatsList = getLocalStorageChatsList();

    chatsMessagesList[currentChatMessages.chatId].push(dataNewMessage);

    chatsList = chatsList
      .map((chat) => {
        if (chat.id === currentChatMessages.chatId) {
          chat.lastMessage = newMessageText;
          chat.lastTypeMessage = type;
          chat.lastDateMessage = currentDateSendMessage;
        }
        return chat;
      })
      .sort((firstChat, secondChat) => new Date(secondChat.lastDateMessage) - new Date(firstChat.lastDateMessage));

    localStorage.setItem("chatsMessagesList", JSON.stringify(chatsMessagesList));

    localStorage.setItem("chatsList", JSON.stringify(chatsList));

    setCurrentChatMessages({
      ...currentChatMessages,
      messages: [...currentChatMessages.messages, dataNewMessage],
    });

    setChatsList(chatsList);

    closeQuotingMessage();

    slideToBottomChatMessage();
  };

  // Открытие блока со смайликами
  const toggleSmileBlock = () => setOpenSmileBox(!openSmileBox);

  // Вставка файла в коретку в инпуте
  const pasteSmile = (unicodeSmile) => {
    let messageField = messageFieldRef.current,
      currentRangeStart = messageField.selectionStart,
      currentRangeEnd = messageField.selectionEnd;

    let ss = document.createElement("div");
    ss.innerHTML = unicodeSmile;

    let pastedText =
      messageField.value.substring(0, currentRangeStart) + ss.innerHTML + messageField.value.substring(currentRangeEnd);

    messageField.value = pastedText;
    messageField.selectionEnd =
      currentRangeStart === currentRangeEnd ? currentRangeEnd + ss.innerHTML.length : currentRangeEnd;
  };

  // Цитирование сообщения
  const setDataQuotingMessages = (quotingMes) => {
    setQuotingMessage({
      active: true,
      message: quotingMes,
    });
  };

  // Закрытие цитируемого сообщения
  const closeQuotingMessage = () =>
    setQuotingMessage({
      active: false,
      message: {},
    });

  // Загрузка файла в сообщение
  const uploadFile = (event) => {
    const currentFile = event.target.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(currentFile);

    reader.onload = function () {
      if (["image/gif", "image/png", "image/jpeg", "image/svg+xml", "image/webp"].indexOf(currentFile.type) > -1) {
        const img = new Image(1, 1);
        img.src = reader.result;
        return sendMessage(img.outerHTML, "img");
      }

      if (["audio/mpeg", "audio/ogg", "audio/ogg"].indexOf(currentFile.type) > -1) {
        const audio = new Audio(reader.result);
        audio.controls = true;
        audio.preload = false;
        return sendMessage(audio.outerHTML, "audio");
      }

      if (["video/mp4", "video/webm", "audio/ogg"].indexOf(currentFile.type) > -1) {
        const video = document.createElement("video");
        video.src = reader.result;
        video.controls = true;
        video.preload = false;
        return sendMessage(video.outerHTML, "video");
      }

      return alert("Тип фалйа не поддерживается");
    };

    reader.onerror = function (error) {
      alert("Произошла ошибка загрузки файла: ", error);
    };

    event.target.value = "";
  };

  // Поиск чата
  const searchChat = () => setSearchingChatList(searchFieldRef.current.value);

  // Русский формат названия типа данных в сообщении ( для навбара )
  const typeTextMessage = {
    img: "Изображение",
    audio: "Аудио",
    video: "Видео",
  };

  const slideToBottomChatMessage = () => {
    const chatBlockWrapper = chatMessageWrapperRef.current;
    if (chatBlockWrapper && currentChatMessages.chatId) chatBlockWrapper.scrollTop = chatBlockWrapper.scrollHeight;
  };

  return (
    <ChatView
      currentUser={user}
      openChat={openChat}
      chatsList={chatsList}
      searchChat={searchChat}
      uploadFile={uploadFile}
      pasteSmile={pasteSmile}
      openSmileBox={openSmileBox}
      createNewChat={createNewChat}
      fileUploadRed={fileUploadRed}
      quotingMessage={quotingMessage}
      searchFieldRef={searchFieldRef}
      messageFieldRef={messageFieldRef}
      typeTextMessage={typeTextMessage}
      toggleSmileBlock={toggleSmileBlock}
      searchingChatList={searchingChatList}
      closeQuotingMessage={closeQuotingMessage}
      currentChatMessages={currentChatMessages}
      handleKeySendMessage={handleKeySendMessage}
      chatMessageWrapperRef={chatMessageWrapperRef}
      setDataQuotingMessages={setDataQuotingMessages}
      handleButtonSendMessage={handleButtonSendMessage}
    />
  );
};

export default ChatContainer;
