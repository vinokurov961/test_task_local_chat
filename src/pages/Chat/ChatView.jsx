import React from "react";
import "../../assets/styles/chat.scss";
import ChatList from "../../components/Chat/ChatList";
import MessageList from "../../components/Chat/MessageList";

const ChatView = (props) => {
  return (
    <>
      <div className="main-navbar">
        <div className="avatarbox"></div>
        <div className="button__main-navbar" onClick={() => alert("Не успел!")}>
          <span className="app-icon__outline"></span>
        </div>
      </div>
      <ChatList {...props} />
      <MessageList {...props} />
    </>
  );
};

export default ChatView;
