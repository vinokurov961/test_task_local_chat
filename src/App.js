import "./assets/styles/App.scss";
import { BrowserRouter } from "react-router-dom";
import { useSelector } from "react-redux";
import RouterConfig from "./navigation/RouterConfig";

const App = () => {
  const { user } = useSelector((state) => state);

  return (
    <BrowserRouter>
      <RouterConfig authChat={user.name ? true : false} />
    </BrowserRouter>
  );
};

export default App;
