import { memo } from "react";

const ChatList = memo(({
  openChat,
  chatsList,
  searchChat,
  createNewChat,
  searchFieldRef,
  typeTextMessage,
  searchingChatList,
  currentChatMessages,
}) => {
  return (
    <div className="main-page-block">
      <div className="app-page mainpage">
        <div className="app-topbar">
          <span className="app-topbar__title">
            <span className="connection-title__online">Чаты</span>
            <div className="write-button" onClick={() => createNewChat()}></div>
          </span>
          <button className="search-field-wrap" type="button" onClick={() => searchChat()}>
            <input className="search-field" type="text" placeholder="Поиск" ref={searchFieldRef} />
            <div className="search-icon"></div>
          </button>
        </div>
        <div className="app-content">
          <div className="recent-list">
            {chatsList.map((chatData) =>
              chatData.chatName.includes(searchingChatList) ? (
                <div
                  key={chatData.id}
                  className={`recent-wrapper ${
                    chatData.id === currentChatMessages.chatId ? "recent-wrapper__current-chat" : ""
                  }`}
                >
                  <div role="button" onClick={() => openChat(chatData.id)} className="dragdrop-area-react"></div>
                  <div className="recent-item">
                    <div className="recent-item__box">
                      <div className="avatarbox avatarbox_text" style={{ background: chatData.chatAvatarColor }}>
                        {chatData.chatName.substring(0, 1)}
                      </div>
                      <div className="recent-item__title">
                        <span>{chatData.chatName}</span>
                      </div>
                      <div className="recent-item__subtitle">
                        <span className="recent-item__lastmsg">
                          {chatData.lastTypeMessage === "text"
                            ? chatData.lastMessage
                            : `${typeTextMessage[chatData.lastTypeMessage]}:`}
                        </span>
                      </div>
                      <div className="recent-item__date">{new Date(chatData.lastDateMessage).toLocaleString()}</div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )
            )}
          </div>
        </div>
      </div>
    </div>
  );
});

export default ChatList;
