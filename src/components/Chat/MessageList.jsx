import { memo } from "react";
import { emojiList } from "../../utils/emojiList";

const MessageList = memo(
  ({
    pasteSmile,
    uploadFile,
    currentUser,
    openSmileBox,
    fileUploadRed,
    quotingMessage,
    messageFieldRef,
    toggleSmileBlock,
    currentChatMessages,
    closeQuotingMessage,
    handleKeySendMessage,
    chatMessageWrapperRef,
    setDataQuotingMessages,
    handleButtonSendMessage,
  }) => {
    return (
      <div className="pane_chat">
        <div className="dialogpage">
          <div
            className={`dialog-page-content ${openSmileBox ? "dialog-page-content__smilebox-open" : ""} ${
              quotingMessage.active ? "dialog-page-content__quoting-open" : ""
            }`}
          >
            <div className={`app-content  ${!currentChatMessages.chatId ? "app-content__chat--noSelected" : ""}`}>
              <div className="chat__shadow">
                <div className="chat__history">
                  <div className="history scrollbar__source">
                    <div className="chat__history-wrap">
                      {currentChatMessages.chatId ? (
                        <div className="chat__messages-wrap" ref={chatMessageWrapperRef}>
                          {currentChatMessages.messages.map((message, index) => (
                            <div
                              key={message.id}
                              className={`message_state-read ${
                                message.authorName !== currentUser.name ? "message_state-read__left" : ""
                              }`}
                              onDoubleClick={() => setDataQuotingMessages(message)}
                            >
                              <span className="message__wrap-text">
                                <span className="message__text">
                                  <div className="message__from__wrapper">
                                    <div className="message__from">
                                      <span>{message.authorName}</span>
                                    </div>
                                  </div>
                                  {Object.keys(message.quoting).length ? (
                                    <div className="message-quote">
                                      <div className="message__from__wrapper">
                                        <div className="message__from">
                                          <span>{message.quoting.authorName}</span>
                                        </div>
                                      </div>
                                      <span className="message__text">
                                        <span className="message__last">
                                          {message.quoting.textType === "text" ? (
                                            message.quoting.text
                                          ) : (
                                            <div
                                              className={`message__last-${message.quoting.textType}`}
                                              dangerouslySetInnerHTML={{ __html: message.quoting.text }}
                                            ></div>
                                          )}
                                        </span>
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  <span className="message__last">
                                    {message.textType === "text" ? (
                                      message.text
                                    ) : (
                                      <div
                                        className={`message__last-${message.textType}`}
                                        dangerouslySetInnerHTML={{ __html: message.text }}
                                      ></div>
                                    )}
                                    <span className="message__dummy-date">{new Date(message.date).toLocaleString()}</span>
                                  </span>
                                  <div className="message__date">{new Date(message.date).toLocaleString()}</div>
                                  <span className="message__status"></span>
                                </span>
                              </span>
                              <div className="message__seens"></div>
                            </div>
                          ))}
                        </div>
                      ) : (
                        <div className="chat__messages-noSelected">Выберите диалог</div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {currentChatMessages.chatId ? (
              <div className="app-bottombar">
                <div className="chat__input">
                  {quotingMessage.active ? (
                    <div className="top-messages">
                      <div className="top-messages__wrap">
                        <div className="top-message quoted-message">
                          <span className="quoted-message__from">{quotingMessage.message.authorName}</span>
                          <span className="quoted-message__text">{quotingMessage.message.text}</span>
                        </div>
                      </div>
                      <div className="top-messages__close" onClick={() => closeQuotingMessage()}>
                        <span></span>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="chat__input-inner">
                    <div className="chat__input-wrap">
                      <input
                        ref={messageFieldRef}
                        className="chat__input-field"
                        placeholder="Сообщение"
                        onKeyPress={(e) => handleKeySendMessage(e)}
                      />
                    </div>
                    <button className="chat__input-sub-action" onClick={() => fileUploadRed.current.click()}>
                      <input className="hide" type="file" ref={fileUploadRed} onChange={(e) => uploadFile(e)} />
                      <span className="chat__input-sub-action__attach"></span>
                    </button>
                    <button className="chat__input-smiles" onClick={() => toggleSmileBlock()} title="Смайлы">
                      <span></span>
                    </button>
                    <button className="chat__input-action" onClick={() => handleButtonSendMessage()}>
                      <span className="chat__input-action__send"></span>
                    </button>
                  </div>
                  <div className="smilebox">
                    <div className="stickerpicker">
                      <div className="scrolled-tabbox__content-wrapper">
                        <div className="scrolled-tabbox__content-block">
                          <div className="scrolled-tabbox__content-scroll">
                            <div className="scrollbars__view">
                              <div className="scrolled-tabbox__content">
                                <div className="stickerpicker__title">Эмодзи</div>
                                {emojiList.map((emoji, indexEmoji) => (
                                  <div
                                    key={indexEmoji}
                                    onMouseDown={() => pasteSmile(emoji)}
                                    className="stickerpicker__item"
                                  >
                                    <span className="emoji_inline" dangerouslySetInnerHTML={{ __html: emoji }}></span>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    );
  }
);

export default MessageList;
