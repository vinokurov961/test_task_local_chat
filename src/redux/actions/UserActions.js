export const SET_USER_DATA = "SET-AUTH-USER-DATA";

export const setUserData = (payload) => ({
  // Change User Auth state
  type: SET_USER_DATA,
  payload,
});
