import { SET_USER_DATA } from "../actions/UserActions";

let initialState = {
  name: "",
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA: {
      return {
        ...state,
        name: action.payload,
      };
    }
    default:
      return state;
  }
};

export default userReducer;
