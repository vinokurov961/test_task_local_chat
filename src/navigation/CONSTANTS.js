// PUBLIC ROUTES
export const ROOT = "/";
export const LOGIN = "/login";
export const NOT_FOUND = "/404";
// AUTH ROUTES TO CHAT

export const CHAT = "/chat";
