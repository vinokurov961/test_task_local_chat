import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { LOGIN, ROOT, CHAT, NOT_FOUND } from "./CONSTANTS";
import LoginContainer from "../pages/Login/LoginContainer";
import NotFoundView from "../pages/NotFound/NotFoundView";
import ChatContainer from "../pages/Chat/ChatContainer";

const RouterConfig = ({ authChat }) => {
  return (
    <Routes>
      <Route path={ROOT} element={<Navigate to={LOGIN} replace />} />
      <Route
        path={LOGIN}
        element={
          <DontRequireAuth redirectTo={CHAT} isAuth={authChat}>
            <LoginContainer />
          </DontRequireAuth>
        }
      />
      <Route
        path={CHAT}
        element={
          <RequireAuth redirectTo={LOGIN} isAuth={authChat}>
            <ChatContainer />
          </RequireAuth>
        }
      />
      <Route path={NOT_FOUND} element={<NotFoundView />} />
      <Route path="*" element={<Navigate to={NOT_FOUND} replace />} />
    </Routes>
  );
};

function RequireAuth({ children, redirectTo, isAuth }) {
  return isAuth ? children : <Navigate to={redirectTo} />;
}

function DontRequireAuth({ children, redirectTo, isAuth }) {
  return isAuth ? <Navigate to={redirectTo} /> : children;
}

export default RouterConfig;
